**BASIC SETUP**

* Download genie.aar file from aar folder

* Import the above file in your project using the below steps(The below steps work with Android Studio)

    * File -> New -> New Module

    * Choose Import .JAR/.AAR Package![image_0.png](https://bitbucket.org/repo/rGb7rX/images/2959591151-image_0.png)

    * Click Next and give the location for the downloaded aar file

    * Give the subproject name as "genie"![image_1.png](https://bitbucket.org/repo/rGb7rX/images/2356885267-image_1.png)

    * Then click Finish

    * Genie module will be imported. You can view it in app’s project view

* In your build.gradle file add the below dependencies

    *  compile 'com.google.android.gms:play-services-gcm:7.8.0 - This dependency should be added only if your app does not include play services as a dependency

    *  compile project(":genie")

* Add the below permissions in your app’s **_app/src/main/AndroidManifest.xml_**

    
```
#!xml

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.READ_PHONE_STATE" />

    <!-- Optional -->
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" /> 
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
```


* In your MainApplication class, add the highlighted line:

    
```
#!java

@Override

    public void onCreate()

    {

        super.onCreate();
        Genie.getInstance().init(this, "{apiKey}", "{secretKey}");
    }
```


* For showing survey banners, we provide you with 2 options

**Option 1 : Show survey banner as a separate activity**


```
#!xml

<!--Add the following code to display surveys -->

	<activity
            android:name="com.genie.offers.activity.BannerActivity"
            android:screenOrientation="portrait"
            android:theme="@style/Theme.Transparent" >
        </activity>

```

	
```
#!java

// Call below api in your activities whenever you want to show the survey. You can add this in multiple places

	public class MyActivity extends AppCompatActivity {

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
		Genie.getInstance().showAd(this);
	    }

        }
```


**Option 2 : Add survey banner in your layout file**


```
#!xml

<!-- Add the below view as a child in your main activity's layout file →

<com.genie.offers.view.GenieBanner
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:id="@+id/genie_image_banner"
        android:layout_alignParentBottom="true" />

```

// In your main main activity file. Add the highlighted code


```
#!java

public class MyActivity extends AppCompatActivity {

	private GenieBanner genieBanner;

	@Override

	protected void onCreate(Bundle savedInstanceState) {

		....

		genieBanner = (GenieBanner) findViewById(R.id.genie_image_banner);
     		genieBanner.loadSurvey();

     		....

    	}

    	@Override

        protected void onDestroy() {

       		super.onDestroy();
                if(genieBanner != null){
 	           genieBanner.destroy();
    		}
        }

   }

```

* Whenever an user clicks on the banner, he/she will be redirected to a web page, where he/she will be prompted with a survey.

* We will pay you for each survey answered by your user. Payout may vary depending on each survey answered